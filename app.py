from fhirclient import client
from fhirclient.models.patient import Patient
from fhirclient.models.medication import Medication
from fhirclient.models.medicationrequest import MedicationRequest 

from flask import Flask, jsonify, session

# app setup
SMART_DEFAULTS = {
    'app_id': '2b98a223-9756-4ee9-96f2-f7d90ccd8538',
    'api_base': 'https://api.logicahealth.org/EHLaboratory/open',
}

app = Flask(__name__)

def _process_patient(smart, patient):
    name = smart.human_name(patient.name[0]) if len(patient.name) > 0 else "Unknown"
    
    address = "Unknown"
    if patient.address != None and len(patient.address) > 0:
        addr = patient.address[0]
        address = f'{addr.line[0]}, {addr.city}, {addr.state}, {addr.country}, {addr.postalCode}'

    return {
        'id': patient.id,
        'name': name,
        'birth_date': patient.birthDate.isostring,
        'address': address,
    }

@app.route('/')
def index():
    """ 
    The app's entrypoint.
    """

    smart = client.FHIRClient(settings=SMART_DEFAULTS)

    bundle = Patient.where({}).perform(smart.server)
    patients = [_process_patient(smart, be.resource) for be in bundle.entry] if bundle is not None and bundle.entry is not None else None

    return jsonify({
        'patients': patients,
        'links': []
    })


# start the app
if '__main__' == __name__:
    app.secret_key = 'LAB_EH_VAD'
    app.run()
